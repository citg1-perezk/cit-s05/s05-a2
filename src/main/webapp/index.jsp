<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder App</title>
</head>
<body>
	<div>
		<h1>Welcome to Servlet JOb Finder!</h1>
		<!--Firstname -->
		<form action="register" method="post">
			<div>
				<label for="firstname">First Name </label>
				<input type="text" name="firstname" required> 
			</div>
		<!--lastname -->
			<div>
				<label for="lastname">Last Name </label>
				<input type="text" name="lastname" required> 
			</div>
		<!--Phone-->
			<div>
				<label for="phone">Phone </label>
				<input type="tel" name="phone" required> 
			</div>
		<!--Email-->
			<div>
				<label for="email">Email </label>
				<input type="email" name="email" required> 
			</div>
			<!--App Discovery -->
				<fieldset>
					<legend>How did you discover the app?</legend>
						<!-- Friends -->
						<input type="radio" name="app_discovery" value="Friends">
						<label>Friends</label>
						<!-- Social Media -->
						<input type="radio" name="app_discovery" value="Social Media">
						<label>Social Media</label>
						<!-- Others -->
						<input type="radio" name="app_discovery" value="others">
						<label>Others</label>
				</fieldset> 
			<!--Date-->
			<div>
				<label for="date">Date of Birth</label>
				<input type="date" name="date" required> 
			</div>
				<div>
					<!-- User Type -->
					<label for="usertype">User Type</label>	
					<select id="usertype" name="user_type">
						<option value="" selected="selected">Select One</option>
						<option value="applicant">Applicant</option>
						<option value="employee">Employee</option>	
					</select>

					
				</div>
				
			<!--Date-->
			<div>
				<label for="profile_description">Profile Description</label>
				<textarea name="profile_description" maxlength="500"></textarea> 
			</div>
			<form action="register" method="post">
				<input type="submit" value="Register">
			</form>
			</form>
		</div>
</body>
</html>