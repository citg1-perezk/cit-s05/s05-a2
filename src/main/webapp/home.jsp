<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Welcome <%=session.getAttribute("fullname")%> !</h1>
	<%
		String userType=session.getAttribute("userType").toString();
		if(userType.equals("applicant")){
			userType="applicant";
			out.println("<p>Welcome "+userType+"."+" You may now start looking for your career opportunity."+"</p>");
		}
		else if(userType.equals("employee")){
			userType="employee";
			out.println("<p>Welcome "+userType+"."+" You may now start browsing applicant profiles."+"</p>");
		}
	%>
	
</body>
</html>